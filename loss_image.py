import argparse
import matplotlib.pyplot as plt
import torch
import numpy as np


# 画Loss图像
def plot_loss(n, flag):
    y = []
    if flag == 0:
        enc = torch.load(f'LeNet_loss_epochs_{n}')
    else:
        enc = torch.load(f'LeNet_FMloss_epochs_{n}')
    enc = np.array(enc)
    for i in range(0, n):
        y.append(enc[i])
    print(y)

    x = list(range(1, len(y) + 1, 1))
    my_x_xticks = np.arange(2, len(y) + 1, 2)
    plt.xticks(my_x_xticks)
    plt.plot(x, y)
    if flag == 0:
        plt_title = f'LeNet Loss Image(dataset:MNIST)(epochs={n})'
    else:
        plt_title = f'LeNet Loss Image(dataset:Fashion MNIST)(epochs={n})'
    plt.title(plt_title)
    plt.xlabel('epochs')
    plt.ylabel('loss value')
    # plt.savefig(file_name)
    plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='loss_image')
    parser.add_argument("-n", type=int, required=True)
    parser.add_argument("-d", required=True, help='0 for MNIST and 1 for Fashion_MNIST')
    args = parser.parse_args()
    n = args.n
    assert args.d == '0' or args.d == '1'
    if args.d == '0':
        flag = 0
    else:
        flag = 1
    plot_loss(n, flag)
