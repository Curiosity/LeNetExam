import argparse

import torch
import torchvision
from torchvision import transforms
from LeNet import LeNet


# 加载测试集
def load_test_data(testset, batch_size):
    test_iter = torch.utils.data.DataLoader(testset, batch_size=batch_size, shuffle=False, num_workers=0)
    return test_iter


# 计算正确率
def evaluate_accuracy(data_iter, net, device=None):
    if device is None and isinstance(net, torch.nn.Module):
        # 如果没指定device就使用net的device
        device = list(net.parameters())[0].device
    acc_sum, n = 0.0, 0
    with torch.no_grad():
        for img, lable in data_iter:
            # print(net(img))
            # print(f'net(img).argmax(dim=1):{net(img).argmax(dim=1)}')
            # print(f'lable:{lable}')
            acc_sum += (net(img).argmax(dim=1) == lable).sum().item()
            n += lable.size(0)
    print(f'测试集总数:{n}个')
    print(f'正确率为:{(100 * acc_sum / n)}%')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='test')
    parser.add_argument("-n", type=int, required=True)
    parser.add_argument("-d", required=True, help='0 for MNIST and 1 for Fashion_MNIST')
    args = parser.parse_args()
    assert args.n > 0
    assert args.d == '0' or args.d == '1'
    num_epochs = args.n

    batch_size = 1000
    mnist_test = torchvision.datasets.MNIST(root='D:/research_works/LeNet/Datasets', train=False, download=False,
                                            transform=transforms.ToTensor())
    fashion_mnist_test = torchvision.datasets.FashionMNIST(root='D:/research_works/LeNet/Datasets', train=False,
                                                           download=False, transform=transforms.ToTensor())

    test_iter = load_test_data(mnist_test, batch_size) if args.d == '0' else load_test_data(
        fashion_mnist_test, batch_size)
    net = LeNet()
    if args.d == '0':
        print('使用的测试集为MNIST')
        net.load_state_dict(torch.load(f'LeNet_mnist_{num_epochs}.pkl', map_location='cpu'))
    else:
        print('使用的测试集为FASHION MNIST')
        net.load_state_dict(torch.load(f'LeNet_fashion_mnist_{num_epochs}.pkl', map_location='cpu'))
    evaluate_accuracy(test_iter, net, device=None)

