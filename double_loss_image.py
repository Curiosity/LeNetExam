import argparse
import matplotlib.pyplot as plt
import torch
import numpy as np


# 画Loss图像
def plot_loss(n):
    y1 = []
    y2 = []
    enc1 = torch.load(f'LeNet_loss_epochs_{n}')
    enc2 = torch.load(f'LeNet_FMloss_epochs_{n}')

    enc1 = np.array(enc1)
    enc2 = np.array(enc2)
    for i in range(0, n):
        y1.append(enc1[i])
        y2.append(enc2[i])
    print(f'y1:{y1}')
    print(f'y2:{y2}')

    x = list(range(1, len(y1) + 1, 1))
    my_x_xticks = np.arange(1, len(y1) + 1, 1)
    plt.xticks(my_x_xticks)
    plt.plot(x, y1, label='MNIST')

    plt.plot(x, y2, label='FashionMNIST')

    plt_title = f'LeNet Loss Image(epochs={n})'
    plt.title(plt_title)
    plt.xlabel('epochs')
    plt.ylabel('loss value')
    # plt.savefig(file_name)
    plt.legend()
    plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='loss_image')
    parser.add_argument("-n", type=int, required=True)
    args = parser.parse_args()
    n = args.n
    plot_loss(n)
