import argparse
import torch
from torch import nn
import torchvision
from torchvision import transforms
import time
from torchsummary import summary

# 加载训练集
def load_train_data(trainset, batch_size):
    train_iter = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=True, num_workers=0)
    return train_iter


# LeNet网络模型
class LeNet(nn.Module):
    def __init__(self):
        super(LeNet, self).__init__()
        self.conv = nn.Sequential(
            nn.Conv2d(in_channels=1, out_channels=6, kernel_size=5),
            nn.Sigmoid(),
            nn.MaxPool2d(kernel_size=2, stride=2),
            nn.Conv2d(in_channels=6, out_channels=16, kernel_size=5),
            nn.Sigmoid(),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )
        self.fc = nn.Sequential(
            nn.Linear(16 * 4 * 4, 120),
            nn.Sigmoid(),
            nn.Linear(120, 84),
            nn.Sigmoid(),
            nn.Linear(84, 10)
        )

    def forward(self, img):
        feature = self.conv(img)
        output = self.fc(feature.view(img.shape[0], -1))
        return output


# 训练函数
def train(net, train_iter, dataset, batch_size, optimizer, device, num_epochs, flag):
    Loss = []
    net = net.to(device)
    print("training on ", device)
    # 损失函数
    criterion = torch.nn.CrossEntropyLoss()
    for epoch in range(num_epochs):
        train_l_sum, train_acc_sum, n, batch_count, start = 0.0, 0.0, 0, 0, time.time()
        for i, (img, label) in enumerate(train_iter):
            output = net(img)
            loss = criterion(output, label)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            if (i + 1) % 10 == 0:
                print('Epoch:[%d/%d],Step:[%d/%d], loss %.4f, time %.1f sec'
                      % (
                          epoch + 1, num_epochs, i + 1, len(dataset) // batch_size, loss.item(), time.time() - start))
        Loss.append(loss.item())
    LossSave = torch.tensor(Loss)
    # 保存Loss值和训练好的模型参数
    if flag == 0:
        torch.save(LossSave, f'LeNet_loss_epochs_{num_epochs}')
        torch.save(net.state_dict(), f'LeNet_mnist_{num_epochs}.pkl')
    else:
        torch.save(LossSave, f'LeNet_FMloss_epochs_{num_epochs}')
        torch.save(net.state_dict(), f'LeNet_fashion_mnist_{num_epochs}.pkl')
    print('训练完成！！！')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='train')
    parser.add_argument("-n", type=int, required=True, help='number of epoch')
    parser.add_argument("-d", required=True, help='0 for MNIST and 1 for Fashion_MNIST')
    args = parser.parse_args()
    assert args.n > 0
    assert args.d == '0' or args.d == '1'
    num_epochs = args.n
    batch_size = 250
    learning_rate = 0.001
    mnist_train = torchvision.datasets.MNIST(root='D:/research_works/LeNet/Datasets', train=True, download=False,
                                             transform=transforms.ToTensor())
    fashion_mnist_train = torchvision.datasets.FashionMNIST(root='D:/research_works/LeNet/Datasets', train=True,
                                                            download=False, transform=transforms.ToTensor())

    train_iter = load_train_data(mnist_train, batch_size) if args.d == '0' else load_train_data(
        fashion_mnist_train, batch_size)
    net = LeNet()
    summary(net, input_size=(1, 28, 28))
    optimizer = torch.optim.Adam(net.parameters(), lr=learning_rate)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    if args.d == '0':
        flag = 0
        dataset = mnist_train
        print('正在训练MNIST数据集...')
    else:
        flag = 1
        dataset = fashion_mnist_train
        print('正在训练FASHION MNIST数据集...')
    train(net, train_iter, dataset, batch_size, optimizer, device, num_epochs, flag)
