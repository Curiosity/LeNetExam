# LeNet网络

一个非常非常远古的卷积神经网络。LeNet神经网络由深度学习三巨头之一的Yan LeCun在1998年提出的，他同时也是卷积神经网络 (CNN，Convolutional Neural Networks)之父。

## 网络结构

![581cfa67cc2ab84a640a0b65d12fc653.png](./_resources/581cfa67cc2ab84a640a0b65d12fc653.png)

## 数据集

本实验在MNIST和FashionMNIST两个数据集上进行。

## 使用方法

+ `LeNet.py` 是模型训练程序，输入1x28x28大小的灰度图，输出是pkl格式模型文件
+ `test.py`是模型测试程序，对测试数据执行测试，输出正确率
+ `loss_image.py`输出单个数据集的loss图像
+ `double_loss_image.py`图像化显示两个数据集loss曲线

## 实验结果

MNIST数据集结果：
|epochs|loss|accuracy|
|---|---|---|
|5 |	0.5617 |85.63%|
|10|	0.1532	|95.98%|
|15|	0.1508	|96.91%|
|20|	0.0955 |97.25%|

FashionMNIST数据集结果：
|epochs	|loss	|accuracy|
|---|---|---|
|5	 |0.848	|67.56%|
|10	|0.6355	|74.50%|
|15	|0.5772	|76.54%|
|20	|0.5198	|78.49%|
